package az.atl.demosecurity2.model;

import az.atl.demosecurity2.dao.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterResponse {
    String fullName;
    String email;
    String password;
    Role role;

    public static RegisterResponse buildRegisterDto(User user) {
        return RegisterResponse.builder()
                .fullName(user.getFullname())
                .email(user.getEmail())
                .password(user.getPassword())
                .role(user.getRole()).build();
    }
}

